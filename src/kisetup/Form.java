package kisetup;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.EventObject;

class Form {

    private static final int WIDTH_MIN = 640;
    private static final int HEIGHT_MIN = 450;
    private static final String FNAME = "KISetup v2017.01.17";
    public enum ACCESS {LOW, NORMAL, HIGH}

    private ACCESS access;

    private TableManager tableManager;
    private Transceiver transceiver;

    private class JTableOwn extends JTable {
        @Override
        public boolean editCellAt(int row, int column, EventObject e) {
            boolean result = super.editCellAt(row, column, e);
            final Component editor = getEditorComponent();
            if (editor == null || !(editor instanceof JTextComponent)) {
                return result;
            }
//            if (e instanceof KeyEvent) { ((JTextComponent) editor).selectAll(); }
            ((JTextComponent) editor).setText("");
            return result;
        }
    }

    private class TableModelOwn extends AbstractTableModel {

        @Override
        public int getRowCount() { return tableManager.getRowCount(); }

        @Override
        public int getColumnCount() {
            int i;
            switch(access){
                case HIGH: i = 0; break;
                default: i = 1;
            }
            return tableManager.getColCount() - i;
        }

        @Override
        public String getColumnName(int columnIndex) { return tableManager.getColName(columnIndex); }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (tableManager.isButton(columnIndex))
                return ButtonColumn.class;
            else
                return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) { return tableManager.isEditable(rowIndex, columnIndex, access == ACCESS.LOW ? 0 : 1); }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) { return tableManager.getValue(rowIndex, columnIndex); }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            try {
                if (columnIndex == 0) {
                    int addr = Integer.parseInt((String)aValue);
                    if (addr > 0 && addr < 256) tableManager.setAddr(addr, rowIndex);
                } else if (columnIndex != 1 && columnIndex != 3) {
                    tableManager.sendValue((String)aValue, rowIndex, columnIndex);
                }
            } catch (NumberFormatException e) { System.out.println(e.getMessage()); }
        }
    }

    private class TableCellRendererOwn extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            setText((String)value);
            setFont(table.getFont());
            setHorizontalAlignment(JLabel.CENTER);
            setVerticalAlignment(JLabel.CENTER);

            setBackground(hasFocus ? Color.CYAN : (row % 2 == 0 ? Color.LIGHT_GRAY : Color.WHITE));
            setBorder(table.getModel().isCellEditable(row, column) ? BorderFactory.createLineBorder(Color.DARK_GRAY, 2) : BorderFactory.createEmptyBorder());

            return this;
        }
    }

    public Form(TableManager tm, Transceiver tr) {
        tableManager = tm;
        transceiver = tr;
    }

    private void buttonPress(String[] buttons, int row, int column) {
        if (buttons.length != 3) return;
        String val;
        if (row == 0) {
            switch (JOptionPane.showOptionDialog(null, buttons[0] + " / " + buttons[1] + " ?", "Set", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, buttons, buttons[2])) {
                case 0: val = "0"; break;
                case 1: val = "1"; break;
                default: return;
            }
        } else {
            switch (tableManager.getValue(row, column)) {
                case "ON": val = "0"; break;
                case "OFF": val = "1"; break;
                case "U": val = "0"; break;
                case "I": val = "1"; break;
                default: return;
            }
        }
        transceiver.sendValue(val, tableManager.getAddrStr(row), tableManager.getColName(column));
    }

    public void createGUI(ACCESS ac) {
        access = ac;

        JFrame frame = new JFrame(FNAME);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension wSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setMinimumSize(new Dimension(WIDTH_MIN, HEIGHT_MIN));
        int width = wSize.width - 80;
        frame.setSize(new Dimension(width, HEIGHT_MIN));
        frame.setLocation((wSize.width - width) / 2, (wSize.height - HEIGHT_MIN) / 2);
//        frame.setResizable(false);

        JPanel panel = new JPanel();
        JComboBox<String> comboBox = new JComboBox<>();
        JButton button_open = new JButton("Open");
        JButton button_close = new JButton("Close");
        button_close.setEnabled(false);

        JPanel panel_2 = new JPanel();
        JCheckBox checkBox_calib = new JCheckBox("Calibration (only first module)");
        JCheckBox checkBox_temp = new JCheckBox("Temperature");

        JTableOwn table = new JTableOwn();
        table.setModel(new TableModelOwn());
        table.setDefaultRenderer(String.class, new TableCellRendererOwn());
        table.setEnabled(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.getTableHeader().setReorderingAllowed(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setRowHeight(30);
        for (int i = 0; i < table.getModel().getColumnCount(); ++i) {
            table.getColumnModel().getColumn(i).setMinWidth(0);
            table.getColumnModel().getColumn(i).setPreferredWidth(tableManager.getColWidth(i));
            table.getColumnModel().getColumn(i).setResizable(false);
        }

        tableManager.setValue("ON/OFF", "0", "SetWork");
        tableManager.setValue("U/I", "0", "SetStab");

        int columnSetWork = 1;
        int columnSetStab = 3;
        new ButtonColumn(table, new AbstractAction() {
            String[] buttons = {"ON", "OFF", "Cancel"};
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonPress(buttons, Integer.parseInt(e.getActionCommand()), columnSetWork);
            }
        }, columnSetWork);
        new ButtonColumn(table, new AbstractAction() {
            String[] buttons = {"U", "I", "Cancel"};
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonPress(buttons, Integer.parseInt(e.getActionCommand()), columnSetStab);
            }
        }, columnSetStab);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        table.setFont(new Font(Font.MONOSPACED, Font.BOLD, 10));

        Insets ins = new Insets(5,5,5,5);
        frame.setLayout(new GridBagLayout());
        frame.add(panel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, ins, 0, 0));
        frame.add(panel_2, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, ins, 0, 0));
        frame.add(scrollPane, new GridBagConstraints(0, 2, 1, 1, 10, 10, GridBagConstraints.NORTH, GridBagConstraints.BOTH, ins, 0, 0));

        panel.setLayout(new GridBagLayout());
        panel.add(comboBox, new GridBagConstraints(0, 0, 1, 1, 10, 10, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, ins, 0, 0));
        panel.add(button_open, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, ins, 0, 0));
        panel.add(button_close, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, ins, 0, 0));

        panel_2.setLayout(new GridBagLayout());
        panel_2.add(checkBox_calib, new GridBagConstraints(0, 0, 1, 1, 10, 10, GridBagConstraints.EAST, GridBagConstraints.NONE, ins, 0, 0));
        panel_2.add(checkBox_temp, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, ins, 0, 0));

        frame.setVisible(true);

        for(String s : transceiver.getPortNames()) comboBox.addItem(s);

        button_open.addActionListener((e) -> {
            if (transceiver.connect(comboBox.getItemAt(comboBox.getSelectedIndex())) == 0) {
                button_open.setEnabled(false);
                button_close.setEnabled(true);
                table.setEnabled(true);
            }
        });

        button_close.addActionListener((e) -> {
            if (transceiver.disconnect() == 0) {
                button_open.setEnabled(true);
                button_close.setEnabled(false);
                table.setEnabled(false);
            }
        });

        checkBox_temp.addItemListener((l) -> tableManager.setTemperatureRead(l.getStateChange() == ItemEvent.SELECTED));

        new Timer(300, (e) -> {
            table.getColumnModel().getColumn(tableManager.getColIdx("Tmp limit")).setPreferredWidth(checkBox_temp.isSelected() ? 100 : 0);
            table.getColumnModel().getColumn(tableManager.getColIdx("Tmp shutdown")).setPreferredWidth(checkBox_temp.isSelected() ? 100 : 0);
            table.getColumnModel().getColumn(tableManager.getColIdx("Up1")).setPreferredWidth(checkBox_calib.isSelected() ? 100 : 0);
            table.getColumnModel().getColumn(tableManager.getColIdx("Up2")).setPreferredWidth(checkBox_calib.isSelected() ? 100 : 0);
            table.getColumnModel().getColumn(tableManager.getColIdx("Ip1")).setPreferredWidth(checkBox_calib.isSelected() ? 100 : 0);
            table.getColumnModel().getColumn(tableManager.getColIdx("Ip2")).setPreferredWidth(checkBox_calib.isSelected() ? 100 : 0);
            table.getColumnModel().getColumn(tableManager.getColIdx("Save")).setPreferredWidth(checkBox_calib.isSelected() || checkBox_temp.isSelected() ? 50 : 0);
            table.repaint();
        }).start();
    }
}
