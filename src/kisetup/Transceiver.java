package kisetup;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

import javax.swing.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.LinkedList;

class Transceiver extends Thread {

    private final Object FOR_SYNC = new Object();

    private boolean connected = false;
    private SerialPort serialPort;

    private WriteBufferManager writeBufferManager = new WriteBufferManager();
    private TableManager tableManager;

    private Timer timer;
    private int timerRowIndex;
    private int timerWait;

    private static class WriteBufferTemplate {
        private static ByteBuffer buff_03 = ByteBuffer.wrap(new byte[]{0x0, 0x03, 0x03, 0x20, 0x00, 0x15, 0x00, 0x00});
        private static ByteBuffer buff_03_3E8 = ByteBuffer.wrap(new byte[]{0x0, 0x03, 0x03, (byte)0xE8, 0x00, 0x01, 0x00, 0x00});
        private static ByteBuffer buff_03_temperature = ByteBuffer.wrap(new byte[]{0x0, 0x03, 0x02, 0x62, 0x00, 0x02, 0x00, 0x00});
        private static ByteBuffer buff_11 = ByteBuffer.wrap(new byte[]{0x0, 0x11, 0x00, 0x00});

        public static ByteBuffer get_03() { return buff_03.duplicate(); }
        public static ByteBuffer get_03_3E8() { return buff_03_3E8.duplicate(); }
        public static ByteBuffer get_03_temperature() { return buff_03_temperature.duplicate(); }
        public static ByteBuffer get_11() { return buff_11.duplicate(); }
    }

    private class WriteBufferManager {
        private volatile LinkedList<ByteBuffer> writeBufferList = new LinkedList<>();

        public boolean isEmpty() { return writeBufferList.isEmpty(); }
        public void addWriteBuffer(ByteBuffer buf) { writeBufferList.add(buf.duplicate()); }
        public ByteBuffer getWriteBuffer() { if (isEmpty()) return null; return  writeBufferList.removeFirst(); }
    }

    private class ValueManager {
        private byte[] val;

        public int bufToInt() { return Byte.toUnsignedInt(val[0]) | Byte.toUnsignedInt(val[1]) << 8 | Byte.toUnsignedInt(val[2]) << 16 | Byte.toUnsignedInt(val[3]) << 24; }
        private void change(int idx1, int idx2) { val[idx1] ^= val[idx2]; val[idx2] ^= val[idx1]; val[idx1] ^= val[idx2]; }
        private void changeDouble() { change(0, 1); change(2, 3); }

        public ValueManager setValue(byte[] buf) { val = ByteBuffer.allocate(4).put(buf).array(); changeDouble(); return this; }
        public ValueManager setValue(int aval) { val = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(aval).array(); changeDouble(); return this; }
        public ValueManager setValue(double aval, double min, double max, int mul) {
            if (aval < min) aval = min;
            else if (aval > max) aval = max;

            val = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int)(aval * mul + 0.5)).array();
            changeDouble();
            return this;
        }

        public String getStringByInt() { return Integer.toString(bufToInt()); }
        public String getStringByDiv(int div) {
            if (div < 1) return "err";
            else return String.format("%.3f", ((double)Integer.toUnsignedLong(bufToInt())) / div);
        }
        public String getStringByDivSigned(int div) {
          if (div < 1) return "err";
          else return String.format("%.3f", ((double)(short)bufToInt()) / div);
        }
        public byte[] getFourBytes() { return Arrays.copyOf(val, 4); }
        public byte[] getTwoBytes() { return Arrays.copyOf(val, 2); }
    }

    public void writeToBuffer(ByteBuffer buff, int row) {
        buff.put(0, (byte) tableManager.getAddr(row));
        writeBufferManager.addWriteBuffer(buff.order(ByteOrder.LITTLE_ENDIAN).
                putShort(buff.array().length - 2, (short) Crc16.getCrc16(buff.array(), buff.array().length - 2)));
    }

    public Transceiver(TableManager tm) {
        tableManager = tm;

        timerRowIndex = 1;
        timerWait = 0;
        timer = new Timer(400, (e) -> {
            if (!tableManager.isTemperatureRead() || timerWait++ == 1) {
                timerWait = 0;

                writeToBuffer(WriteBufferTemplate.get_03(), timerRowIndex);
                writeToBuffer(WriteBufferTemplate.get_03_3E8(), timerRowIndex);
                writeToBuffer(WriteBufferTemplate.get_11(), timerRowIndex);
                if (tableManager.isTemperatureRead()) writeToBuffer(WriteBufferTemplate.get_03_temperature(), timerRowIndex);

                if (++timerRowIndex == tableManager.getRowCount()) timerRowIndex = 1;
            }
        });
    }

    public String[] getPortNames() { return SerialPortList.getPortNames(); }

    public int connect(String portName) {

        serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
            serialPort.setParams(
                    SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE
            );
            timer.start();
            connected = true;
            return 0;
        }
        catch (SerialPortException e) { System.out.println(e.getMessage()); return -1; }
    }

    public int disconnect() {
        synchronized (FOR_SYNC) {
            try {
                serialPort.closePort();
                timer.stop();
                connected = false;
                return 0;
            } catch (SerialPortException e) {
                System.out.println(e.getMessage());
                return -1;
            }
        }
    }

    public void sendValue(String newVal, String addr, String colName) {
        int len = 13;
        byte[] buf = new byte[len];
        byte[] temp;
        ValueManager valueManager = new ValueManager();

        double val = 0;
        try {
            if (!colName.equals("ID")) val = Double.parseDouble(newVal);
            buf[0] = (byte)Integer.parseInt(addr);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return;
        }
        buf[1] = 0x10;
        buf[4] = 0x00;
        buf[5] = 0x02;
        buf[6] = 0x04;

        switch (colName) {
            case "SetWork":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x02;
                buf[3] = (byte)0xB9;
                buf[4] = 0;
                buf[5] = (byte)val;
                break;
            case "SetStab":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x03;
                buf[3] = (byte)0xED;
                buf[4] = 0;
                buf[5] = (byte)(val == 0 ? 0x01 : 0x03);
                break;
            case "Uset":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x03;
                buf[3] = (byte)0xEC;
                temp = valueManager.setValue(val, 0, 100, 100).getTwoBytes();
                buf[4] = temp[0];
                buf[5] = temp[1];
                break;
            case "Iset":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x03;
                buf[3] = (byte)0xEB;
                temp = valueManager.setValue(val, 0, 100, 100).getTwoBytes();
                buf[4] = temp[0];
                buf[5] = temp[1];
                break;
            case "Tmp limit":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x02;
                buf[3] = (byte)0x62;
                temp = valueManager.setValue(val, 0, 150, 1).getTwoBytes();
                buf[4] = temp[0];
                buf[5] = temp[1];
                break;
            case "Tmp shutdown":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x02;
                buf[3] = (byte)0x63;
                temp = valueManager.setValue(val, 0, 150, 1).getTwoBytes();
                buf[4] = temp[0];
                buf[5] = temp[1];
                break;
            case "Up1":
                buf[2] = 0x02;
                buf[3] = (byte)0xBC;
                temp = valueManager.setValue(val, 0, 120, 1000).getFourBytes();
                buf[7] = temp[0];
                buf[8] = temp[1];
                buf[9] = temp[2];
                buf[10] = temp[3];
                break;
            case "Up2":
                buf[2] = 0x02;
                buf[3] = (byte)0xBE;
                temp = valueManager.setValue(val, 0, 120, 1000).getFourBytes();
                buf[7] = temp[0];
                buf[8] = temp[1];
                buf[9] = temp[2];
                buf[10] = temp[3];
                break;
            case "Ip1":
                buf[2] = 0x02;
                buf[3] = (byte)0xC8;
                temp = valueManager.setValue(val, 0, 120, 1000).getFourBytes();
                buf[7] = temp[0];
                buf[8] = temp[1];
                buf[9] = temp[2];
                buf[10] = temp[3];
                break;
            case "Ip2":
                buf[2] = 0x02;
                buf[3] = (byte)0xCA;
                temp = valueManager.setValue(val, 0, 120, 1000).getFourBytes();
                buf[7] = temp[0];
                buf[8] = temp[1];
                buf[9] = temp[2];
                buf[10] = temp[3];
                break;
            case "Save":
                len = 8;
                buf[1] = 0x6;
                buf[2] = 0x02;
                buf[3] = (byte)0xBB;
                buf[4] = 0;
                buf[5] = 0;
                break;
            case "ID":
                buf[2] = 0x02;
                buf[3] = (byte)0xDA;
                String[] id_string = newVal.trim().split(" ");
                if (id_string.length == 2) {
                  int year;
                  int id;

                  try {
                    year = Integer.parseInt(id_string[0]);
                    if (year < 0 || year > 255) return;
                    id = Integer.parseInt(id_string[1]);
                    if (id < 0 || id > 65535) return;
                  } catch (NumberFormatException e) {
                    System.out.print(e.getMessage());
                    return;
                  }

                  buf[7] = (byte) year;
                  buf[8] = 0;
                  temp = valueManager.setValue(id).getTwoBytes();
                  buf[9] = temp[1];
                  buf[10] = temp[0];
                } else if (id_string.length == 3) {
                  int card_voltage;
                  try {
                    if (Integer.parseInt(id_string[0]) != 0) return;
                    if (Integer.parseInt(id_string[1]) != 0) return;
                    card_voltage = Integer.parseInt(id_string[2]);
                  } catch (NumberFormatException e) {
                    System.out.print(e.getMessage());
                    return;
                  }

                  len = 8;
                  buf[1] = 0x6;
                  buf[2] = 0x02;
                  buf[3] = (byte)0xB8;
                  buf[4] = 0;
                  buf[5] = (byte)card_voltage;
                } else return;
//                System.out.println(Arrays.toString(buf));
                break;
            case "IDa":
                buf[2] = 0x02;
                buf[3] = (byte)0xDC;
                temp = valueManager.setValue((int)val).getFourBytes();
                buf[7] = temp[0];
                buf[8] = temp[1];
                buf[9] = temp[2];
                buf[10] = temp[3];
                break;
            default:
                return;
        }

        int crc = Crc16.getCrc16(buf, len - 2);
        buf[len - 2] = (byte)(crc & 0xFF);
        buf[len - 1] = (byte)((crc >> 8) & 0xFF);
        writeBufferManager.addWriteBuffer(ByteBuffer.wrap(Arrays.copyOf(buf, len)));
        timer.restart();
    }

    private int readIdx;
    private byte[] getArray(byte[] buf, int len) {
        readIdx += len;
        return Arrays.copyOfRange(buf, readIdx - len, readIdx);
    }

    private boolean parseMessage(byte[] readBuf) {
        if (readBuf == null) return false;
        if (readBuf.length < 4) return false;
        if (readBuf.length >= 2 && (Byte.toUnsignedInt(readBuf[1]) & 0x80) == 0x80) { System.out.println("Error Pack: " + Arrays.toString(readBuf)); return false; }

        int crcMust = (Byte.toUnsignedInt(readBuf[readBuf.length - 2]) | Byte.toUnsignedInt(readBuf[readBuf.length - 1]) << 8) & 0xFFFF;
        int crc = Crc16.getCrc16(readBuf, readBuf.length - 2);
        if (crc != crcMust) return false;

        String addr = Integer.toString(Byte.toUnsignedInt(readBuf[0]));
        ValueManager valueManager = new ValueManager();

        switch (Byte.toUnsignedInt(readBuf[1])) {
            case 0x03:
                switch (readBuf.length) {
                    case 47: //addr + func + bytes count + registers + crc
                        if (Byte.toUnsignedInt(readBuf[2]) != 42) return false; //byte count
                        readIdx = 3;
                        if (valueManager.setValue(getArray(readBuf, 2)).getStringByInt().equals("0")) {
                            tableManager.setValue("I", addr, "SetStab");
                            tableManager.setValue("Напряжение", addr, "Stab");
                        } else {
                            tableManager.setValue("U", addr, "SetStab");
                            tableManager.setValue("Ток", addr, "Stab");
                        }
//                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByInt().equals("0") ? "Напряжение" : "Ток", addr, "Stab");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByDiv(100), addr, "Uset");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByDiv(100), addr, "Iset");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByDiv(100), addr, "Uin");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByDiv(100), addr, "Ucentr");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByDiv(1000), addr, "Uout");
                        int u = valueManager.bufToInt();
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByDiv(1000), addr, "Iout");
                        int i = valueManager.bufToInt();

                        try { tableManager.setValue(String.format("%.3f", (double)u / 1000 * (double)i / 1000), addr, "Wout"); }
                        catch (Exception e) { tableManager.setValue("-", addr, "Wout"); System.out.println(e.getMessage() + " " + u + " " + i); }

                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByDivSigned(100), addr, "Tmp");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByDiv(1000), addr, "Up1");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByDiv(1000), addr, "Up2");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByDiv(1000), addr, "Ip1");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByDiv(1000), addr, "Ip2");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByInt(), addr, "Save");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 4)).getStringByInt(), addr, "IDa");

                        if (!tableManager.isTemperatureRead()) {
                            tableManager.setValue("-", addr, "Tmp limit");
                            tableManager.setValue("-", addr, "Tmp shutdown");
                        }

                        break;
                    case 9:
                        if (Byte.toUnsignedInt(readBuf[2]) != 4) return false; //byte count
                        readIdx = 3;
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByInt(), addr, "Tmp limit");
                        tableManager.setValue(valueManager.setValue(getArray(readBuf, 2)).getStringByInt(), addr, "Tmp shutdown");
                        break;
                    case 7:
                        if (Byte.toUnsignedInt(readBuf[2]) != 2) return false; //byte count
                        readIdx = 3;
                        byte[] val = valueManager.setValue(getArray(readBuf, 2)).getFourBytes();
                        if ((val[0] & 0x1) != 0) {
                            tableManager.setValue("OFF", addr, "SetWork");
                            tableManager.setValue("ON", addr, "Work");
                        } else {
                            tableManager.setValue("ON", addr, "SetWork");
                            tableManager.setValue("OFF", addr, "Work");
                        }
                        String status;
                        if ((val[0] & 0x10) != 0 || (val[1] & 0x1) != 0) status = "";
                        else status = "OK";
                        if ((val[0] & 0x10) != 0) status += "PwrLim";
                        if ((val[1] & 0x1) != 0) status += " HiTmp";
                        if ((val[0] & 0x8) != 0) status += " Ictrl";
                        tableManager.setValue(status, addr, "Status");
                        break;
                    default: return false;
                }
                break;
            case 0x11:
                if (readBuf.length != 24) return false; //addr + func + bytes count + ID + crc
                if (Byte.toUnsignedInt(readBuf[2]) != 19) return false; //byte count
                StringBuilder sb = new StringBuilder();
                for (int i = 3; i < readBuf.length - 5; ++i) sb.append((char)readBuf[i]);
                sb.append(String.format("_%02d", Byte.toUnsignedInt(readBuf[readBuf.length - 5])));
                sb.append(String.format("_%05d", Short.toUnsignedInt(ByteBuffer.wrap(Arrays.copyOfRange(readBuf, readBuf.length - 4, readBuf.length - 2)).getShort())));
                tableManager.setValue(sb.toString(), addr, "ID");
                break;
            default:
                return false;
        }
        return true;
    }

    private void msleep(long msec) { try { sleep(msec); } catch (InterruptedException e) { System.out.println(e.getMessage()); } }

    @Override
    public void run() {
        boolean readAvailable = false;
        ByteBuffer forWrite;
        int addr = 1;

        //noinspection InfiniteLoopStatement
        while (true) {
            msleep(1);
            if (!connected) continue;
            synchronized (FOR_SYNC) {
                try {
                    if (readAvailable) {
                        readAvailable = false;
                        if (!parseMessage(serialPort.readBytes())) tableManager.noAnswer(addr);
                    } else if (!writeBufferManager.isEmpty()) {
                        serialPort.purgePort(SerialPort.PURGE_RXABORT | SerialPort.PURGE_RXCLEAR | SerialPort.PURGE_TXABORT | SerialPort.PURGE_TXCLEAR);
                        forWrite = writeBufferManager.getWriteBuffer();
                        if (forWrite == null) continue;
                        if (forWrite.get(1) == 0x03 || forWrite.get(1) == 0x11) {
                            addr = forWrite.get(0);
                            readAvailable = true;
                        }
                        serialPort.writeBytes(forWrite.array());
                        msleep(100);
                    }
                } catch (SerialPortException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
