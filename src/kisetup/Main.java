package kisetup;

import javax.swing.*;
import java.util.Arrays;

public class Main {

    private static int getAccessByPass(char[] pass) {
        int[] actualPass = { 1564171397, 50492665, 49 };

        StringBuilder sb = new StringBuilder();
        Arrays.asList(pass).forEach(sb::append);
//        System.out.println(sb.toString().hashCode());

        int ret = -1;
        for (int i = 0; i < actualPass.length; ++i) if (sb.toString().hashCode() == actualPass[i]) ret = i;

        Arrays.fill(pass, '0');
        Arrays.fill(actualPass, 0);
        sb.delete(0, sb.length());

        return ret;
    }

    public static void main(String[] args) {
        JPasswordField passField = new JPasswordField();
        final Form.ACCESS access;
        while (true) {
            int ret = JOptionPane.showConfirmDialog(null, passField, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            int passId = getAccessByPass(passField.getPassword());
            passField.setText("");
            if (ret == JOptionPane.CANCEL_OPTION || ret == JOptionPane.CLOSED_OPTION) System.exit(0);
            switch (passId) {
                case 0: access = Form.ACCESS.HIGH; break;
                case 1: access = Form.ACCESS.NORMAL; break;
                case 2: access = Form.ACCESS.LOW; break;
                default: continue;
            }
            break;
        }

        TableManager tableManager = new TableManager();

        Transceiver transceiver = new Transceiver(tableManager);
        tableManager.setValueSender(transceiver::sendValue);

        Form form = new Form(tableManager, transceiver);
        javax.swing.SwingUtilities.invokeLater(() -> form.createGUI(access));

        transceiver.setPriority(Thread.MIN_PRIORITY);
        transceiver.start();
    }
}
