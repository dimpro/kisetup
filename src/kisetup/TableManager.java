package kisetup;

import java.util.ArrayList;
import java.util.Arrays;

class TableManager {
    private static final int MODULES = 6;
    private static final int NO_ANS_CLEAN_COUNT = 4;
    private static final int ROW_COUNT = MODULES + 1;

    private volatile ArrayList<ArrayList<String>> tableData = new ArrayList<>(ROW_COUNT);
    private int[] noAnsCounter = new int[ROW_COUNT];

    private boolean temperatureRead = false;

    private ColumnsManager colManager = new ColumnsManager();
    private class ColumnsManager {
        private ArrayList<String> col_names = new ArrayList<>(50);
        private ArrayList<Integer> col_width = new ArrayList<>(50);
        private ArrayList<String> col_params = new ArrayList<>(50);

        public void addColumn(String name, String params) {
            addColumn(name, params, 90);
        }
        public void addColumn(String name, String params, int width) {
            col_names.add(name);
            col_params.add(params);
            col_width.add(width);
        }

        public int getColumnsCount() { return col_names.size(); }
        public String getColumnName(int idx) { return col_names.get(idx); }
        public int findColumnByName(String name) { return col_names.indexOf(name); }
        public String getParams(int idx) { return col_params.get(idx); }
        public int getWidth(int idx) { return col_width.get(idx); }
    }

    private ValueSender valueSender;
    @FunctionalInterface
    public interface ValueSender {
        void sendValue(String newVal, String addr, String colName);
    }

    //params have one character for every level access
    //r - readonly, w - writeable, 1 - writeable only 1 row, b - button (writeable all rows), a - writeable all rows
    public TableManager() {
        colManager.addColumn("Addr", "ww", 40);
        colManager.addColumn("SetWork", "bb", 90);
        colManager.addColumn("Work", "rr");
        colManager.addColumn("SetStab", "bb", 60);
        colManager.addColumn("Stab", "rr");
        colManager.addColumn("Uset", "aa");
        colManager.addColumn("Iset", "aa");
        colManager.addColumn("Uin", "rr");
        colManager.addColumn("Ucentr", "rr");
        colManager.addColumn("Uout", "rr");
        colManager.addColumn("Iout", "rr");
        colManager.addColumn("Wout", "rr");
        colManager.addColumn("Tmp", "rr");
        colManager.addColumn("ID", "rw", 180);
        colManager.addColumn("Status", "rw", 120);
        colManager.addColumn("Tmp limit", "r1");
        colManager.addColumn("Tmp shutdown", "r1");
        colManager.addColumn("Up1", "r1");
        colManager.addColumn("Up2", "r1");
        colManager.addColumn("Ip1", "r1");
        colManager.addColumn("Ip2", "r1");
        colManager.addColumn("Save", "rw", 50);
        colManager.addColumn("IDa", "rw");

        for (int i = 0; i < ROW_COUNT; ++i) {
            tableData.add(new ArrayList<>(Arrays.asList(new String[colManager.getColumnsCount()])));
            tableData.get(i).set(0, Integer.toString(i));
        }
    }

    public void setValueSender(ValueSender vs) { valueSender = vs; }

    public void setTemperatureRead(boolean val) { temperatureRead = val; }
    public boolean isTemperatureRead() { return temperatureRead; }

    public int getRowCount() { return ROW_COUNT; }
    public int getColCount() { return colManager.getColumnsCount(); }

    public int getColIdx(String name) { return colManager.findColumnByName(name); }
    public String getColName(int index) { return colManager.getColumnName(index); }
    public boolean isEditable(int rowIndex, int columnIndex, int access_level) {
        char ch = colManager.getParams(columnIndex).charAt(access_level);
        return (ch == 'w' && rowIndex > 0) || ch == 'b' || ch == 'a' || (ch == '1' && rowIndex == 1);
    }
    public boolean isButton(int columnIndex) {return colManager.getParams(columnIndex).charAt(0) == 'b';}
    public int getColWidth(int colIndex) { return colManager.getWidth(colIndex); }

    public String getAddrStr(int rowIndex) { return tableData.get(rowIndex).get(0); }
    public int getAddr(int rowIndex) {
        try {
            return Integer.parseInt(getAddrStr(rowIndex));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }
    public void setAddr(int addr, int rowIndex) { tableData.get(rowIndex).set(0, Integer.toString(addr)); }

    public void sendValue(String val, int rowIndex, int colIndex) { valueSender.sendValue(val, getAddrStr(rowIndex), colManager.getColumnName(colIndex)); }

    public String getValue(int rowIndex, int colIndex) { return tableData.get(rowIndex).get(colIndex); }
    public void setValue(String val, String addr, String colName) {
        int row = -1;
        for (int i = 0; i < ROW_COUNT; ++i) if (tableData.get(i).get(0).equals(addr)) row = i;
        if (row == -1) return;
        int column = colManager.findColumnByName(colName);
        if (column == -1) return;
        tableData.get(row).set(column, val);
    }

    public void noAnswer(int addr) {
        for(int row = 0; row < ROW_COUNT; ++row) {
            if (tableData.get(row).get(0).equals(Integer.toString(addr))) {
                if (++noAnsCounter[row] == NO_ANS_CLEAN_COUNT) {
                    noAnsCounter[row] = 0;
                    for (int col = 1; col < tableData.get(row).size(); ++col)
//                        if (!isButton(col))
                            tableData.get(row).set(col, "-");
                }
                break;
            }
        }
    }
}
